/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': { view: 'pages/homepage' },

  /** USER */
  'post /api/signup': 'UsersController.addUser',
  'post /api/login': 'UsersController.getUserByUserName',
  'get /api/user/:id': 'UsersController.getUserByUserID',

  /** PRODUCT */
  'get /api/product': 'ProductController.getAllProduct',
  'get /api/product/:id': 'ProductController.getProduct',
  'post /api/product/add-product': 'ProductController.addProduct',
  'get /api/product/recent-product': 'ProductController.getRecentAddedProduct',
  'post /api/product/rate-product': 'ProductController.rateProduct',
  'get /api/product/get-product-by-filter': 'ProductController.getProductByFilters',

  /** CART */
  'post /api/add-to-cart': 'CartController.addCartItem',
  'post /api/cart': 'CartController.getCartItem',
  'get /api/cart-remove/:id': 'CartController.deleteCartItem',

  /** ADDRESS */
  'post /api/addAddress': 'AddressController.addAddress',
  'get /api/address/:id': 'AddressController.getAddress',
  'put /api/editAddress': 'AddressController.updatedAddress',

  /**Category */
  'get /api/category': 'CategoryController.getCategory',

  /** SUB CATEGORY */
  'get /api/subcategory/:id': 'SubCategoryController.getSubCategory',

  /** PAYMENT */
  'post /api/payment/request': 'PaymentController.generatePymentRequest',

  /** ORDER */
  'post /api/place-order': 'OrderController.createOrder',
  'get /api/get-order-details/:id': 'OrderController.getOrderDetails',
  'get /api/get-order/:id': 'OrderController.getOrderList',

  /**COMMENTS */
  'get /api/comment/:id': 'CommentsController.getComments',
  'post /api/comment': 'CommentsController.addComment',

  /** BRAND */
  'get /api/brand': 'BrandController.getBrand',



  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/


};
