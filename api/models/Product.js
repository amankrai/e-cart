/**
 * Product.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    //Primitive
    productName: { type: 'string', required: true },
    productDescription: { type: 'string', required: true },
    price: { type: 'number', required: true },
    available: { type: 'number', required: true },

    //Associative
    images: { collection: 'images', via: 'product_id' },
    comments: { collection: 'Comments', via: 'product_id' },
    category_id: { model: 'category' },
    subCategory_id: { model: 'SubCategory' },
    brand_id: { model: 'brand' },
    userProductMappingId: { collection: 'UserProductMapping', via: 'product_id' },
    cart: { collection: 'cart', via: 'product_id' }
  },

};

