module.exports = {
  attributes: {
    name: { type: 'string', required: true, },
    userName: { type: 'string', required: true, unique: true },
    emailAddress: { type: 'string', required: true, unique: true },
    accountType: { type: 'string', required: true },
    profilePhoto: { type: 'string' },
    password: { type: 'string', required: true, encrypt: true },
    cart: {
      collection: 'cart',
      via: 'user_id'
    },
    comment: {
      collection: 'Comments',
      via: 'user_id'
    },
    address: {
      collection: 'address',
      via: 'user_id'
    },
    userProductMapping: {
      collection: 'UserProductMapping',
      via: 'user_id'
    }
  },
};

