/**
 * Address.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    addressLine1: { type: 'string', required: true },
    addressLine2: { type: 'string' },
    street: { type: 'string', required: true },
    locality: { type: 'string', required: true },
    city: { type: 'string', required: true },
    state: { type: 'string', required: true },
    country: { type: 'string', required: true },
    postalCode: { type: 'number', required: true },
    addressType: { type: 'string', required: true },
    user_id: { model : 'Users'}
  },

};

