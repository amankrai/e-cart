module.exports = {
  attributes: {
    product_id: { model: 'Product' },
    quantity: { type: 'number', required: true },
    user_id: { model: 'Users' },
    active: { type: 'Boolean', required: true },
    variant_id: { model: "Variants" },
    variation_index: { type: 'number' }
  },
};

