/**
 * Category.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    name: { type: 'string', required: true },
    product: {
      collection: 'Product', via: 'category_id'
    },
    subCategory: {
      collection: 'SubCategory', via: 'category_id'
    }
  },

};

