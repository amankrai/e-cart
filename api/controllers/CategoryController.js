/**
 * CategoryController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

    getCategory: function (req, res) {
        Category.find().exec(function (err, data) {
            if (data) {
                data.forEach(element => {
                    delete element.createdAt;
                    delete element.updatedAt;
                });
                res.send({
                    results: data
                });
            }
            else if (err) {
                res.send({
                    error: err
                });
            }
            else {
                res.send({
                    message: 'No data to display'
                })
            }
        });
    }

};

