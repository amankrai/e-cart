/**
 * OrderController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
    createOrder: async function (req, res) {
        const id = parseInt(req.body.user_id);

        const orderParams = {
            data: {},
            payment_id: req.body.payment_id,
            payment_status: req.body.payment_status,
            user_id: id
        }
        const user = {
            user_id: id,
            active: true
        };
        await Order.find({ payment_id: req.body.payment_id }).exec(async function (err, orderItem) {
            if (orderItem.length > 0) {
                res.send({
                    error: "Sorry something went wrong while making payment. Please try again."
                });
            }
            else if (err) {
                res.sedn({
                    error: err,
                });
            }
            else {
                Cart.find(user).exec(async function (err, cartItem) {
                    if (cartItem.length > 0) {
                        cartItem.map((item, key) => {
                            orderParams.data[key] = {
                                product_id: item.product_id,
                                quantity: item.quantity
                            }
                            return;
                        });
                        console.log(orderParams)
                        const order = await Order.create(orderParams).fetch();
                        if (order) {
                            res.send({
                                results: order
                            });
                        }
                        else {
                            res.send({
                                error: 'fail to create order'
                            });
                        }
                    }
                    else if (err) {
                        res.send({
                            error: err
                        });
                    }
                });
            }
        });
    },
    getOrderDetails: function (req, res) {
        const orderId = parseInt(req.params.id);
        const params = {
            id: orderId
        }
        Order.findOne(params).exec(async function (err, orderObject) {
            if (orderObject) {
                delete orderObject.createdAt;
                delete orderObject.updatedAt;
                const temp = Object.values(orderObject.data).map(async (dataObject) => {
                    const product_id = dataObject.product_id;
                    const products = await Product.findOne({ id: product_id }).populateAll();
                    if (products) {
                        dataObject.productValue = products;
                    }
                    return dataObject;
                });
                const orderObj = await Promise.all(temp);
                res.send({ results: orderObject })
            }
            else if (err) {
                res.send({
                    error: err
                })
            }

        });
    },
    getOrderList: function (req, res) {
        const id = parseInt(req.params.id);
        const params = {
            user_id: id
        }
        Order.find(params).exec(function (err, data) {
            if (data) {
                res.send({
                    results: data
                });
            }
            else if (err) {
                res.send({
                    error: err
                });
            }
        });
    }
};

