/**
 * PaymentController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
var Insta = require('instamojo-nodejs');
Insta.setKeys('test_23ab27acf64a4e1248201441244', 'test_6ca88a61690e9300f21de2a112a');
Insta.isSandboxMode(true);

module.exports = {
    generatePymentRequest: function (req, res) {
        var data = new Insta.PaymentData();

        data.purpose = req.body.purpose;// required
        data.amount = req.body.amount;  // required
        data.currency = 'INR';
        data.buyer_name = req.body.buyer_name;
        data.email = req.body.email;
        data.phone = req.body.phone;
        data.send_email = req.body.send_email;
        data.send_sms = req.body.send_sms;
        data.allow_repeated_payments = false;
        data.redirect_url = `http://localhost:3000/orderConfirmation?user_id=${req.body.user_id}&`;

        Insta.createPayment(data, function (error, response) {
            if (error) {
                res.send({
                    error: error
                })
            } else {
                // Payment redirection link at response.payment_request.longurl
                response = JSON.parse(response);
                let longurl = response.payment_request.longurl;
                let status = response.payment_request.status
                res.send({
                    url: longurl,
                    status: status
                })
            }
        });
    },

};

