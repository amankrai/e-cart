module.exports = {

    addProduct: async function (req, res) {
        console.log(req.body)
        let category = req.body.category;
        let subCategory = req.body.subCategory;
        let brand = req.body.brand;
        if (typeof category !== 'number') {
            const newCategory = await Category.create({ name: category }).fetch();
            if (newCategory) {
                category = newCategory.id;
            }
        }
        if (typeof brand !== 'number') {
            const newBrand = await Brand.create({ name: brand }).fetch();
            if (newBrand) {
                brand = newBrand.id;
            }
        }
        if (typeof subCategory !== 'number') {
            const newSubCategory = await SubCategory.create({ name: subCategory, category_id: category }).fetch();
            if (newSubCategory) {
                subCategory = newSubCategory.id;
            }
        }
        const productParams = {
            productName: req.body.productName,
            productDescription: req.body.productDescription,
            price: req.body.price,
            available: req.body.available,
            category_id: category,
            subCategory_id: subCategory,
            brand_id: brand
        };
        const newProduct = await Product.create(productParams).fetch();
        if (newProduct) {
            const newImage = await Images.create({ data: req.body.image, product_id: newProduct.id }).fetch();
            const newUserProductMapping = await UserProductMapping.create({ user_id: req.body.userId, product_id: newProduct.id }).fetch();
            if (newImage && newUserProductMapping) {
                res.send({
                    message: 'Product Added successfully',
                    results: newProduct,
                })
            }
            else {
                res.send({
                    message: 'Something went wrong while inserting data in image or mapping table',
                })
            }
        }
        else {
            res.send({
                message: 'Something went wrong while inserting data in product table',
            })
        }
    },
    getAllProduct: function (req, res) {
        Product.find().populate('images').exec(function (err, data) {
            if (data.length > 0) {
                res.send({
                    results: data,
                })
            }
            else {
                res.send({
                    message: err,
                });
            }
        });
    },
    getProductByFilters: function (req, res) {
        const keys = Object.keys(req.query);
        const values = Object.values(req.query);
        let params = {};
        keys.map((keyValue, key) => {
            if (keyValue == "category_id" || keyValue == "subCategory_id") {
                params[keyValue] = parseInt(values[key]);
            }
            return;
        });
        Product.find(params).populate('images').exec(function (err, data) {
            if (data) {
                res.send({
                    results: data,
                })
            }
            else {
                res.send({
                    message: err,
                });
            }
        });
    },
    getProduct: function (req, res) {
        let productId = parseInt(req.params.id);
        const param = {
            id: productId,
        };
        Product.findOne(param).populateAll().exec(async function (err, data) {
            if (data) {
                let params = { id: data.userProductMappingId[0].id };
                UserProductMapping.findOne(params).populateAll().exec(async function (err, userData) {
                    if (userData) {
                        const subCategory = await SubCategory.findOne({ id: data.subCategory_id.id }).populate('variant_id');
                        if (subCategory.variant_id) {
                            data.variant = subCategory.variant_id;
                        }
                        data.seller = userData.user_id.name;
                        delete data.userProductMappingId;
                        res.send({
                            results: data,
                        })
                    }
                    else {
                        req.send({
                            error: err
                        })
                    }

                })
            }
            else if (err) {
                res.send({
                    error: err,
                });
            }
            else {
                res.send({
                    message: "No Product to show",
                });
            }
        });
    },
    getRecentAddedProduct: async function (req, res) {
        const productData = await Product.find().populateAll().sort('id DESC').limit(5);
        if (productData) {
            res.send({
                results: productData
            });
        }
        else {
            res.send({
                error: "Unable to fetch data from table."
            })
        }
    },
};
