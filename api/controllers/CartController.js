/**
 * CartController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

    addCartItem: function (req, res) {
        const variant_id = parseInt(req.body.variant_id);
        const variant_index = parseInt(req.body.variant_index)
        let cartParams = {
            product_id: req.body.product_id,
            user_id: req.body.user_id,
            active: true,
            variant_id: variant_id,
            variant_index: variant_index
        }
        Cart.findOne(cartParams).exec(async function (err, cart) {
            if (cart) {
                let updatedQuantity = cart.quantity + req.body.quantity;
                const updatedCartItem = await Cart.updateOne(cartParams).set({ quantity: updatedQuantity });
                if (updatedCartItem) {
                    res.send({
                        message: "successfully updated the item in cart",
                        results: updatedCartItem
                    })
                }
                else {
                    req.send({
                        error: "Failed updating products in your cart",
                    })
                }
            }
            else {
                cartParams['quantity'] = req.body.quantity;
                const newCartItem = await Cart.create(cartParams).fetch();
                if (newCartItem) {
                    res.send({
                        message: "successfully added the item in cart",
                        results: newCartItem
                    })
                }
                else {
                    req.send({
                        error: "Failed adding products in your cart",
                    })
                }
            }
        })
    },
    getCartItem: function (req, res) {
        const params = {
            user_id: req.body.user_id,
            active: true
        }
        Cart.find(params).populate('product_id').exec(async function (err, cartItem) {
            if (cartItem.length > 0) {
                const cartItemList = cartItem.map(async item => {
                    const imageData = await Images.findOne({ product_id: item.product_id.id });
                    if (imageData) {
                        item.product_id.image = imageData.data;
                    }
                    else {
                        item.product_id.image = null;
                    }
                    return item;
                });
                const cartItemObject = await Promise.all(cartItemList);
                res.send({
                    results: cartItemObject,
                });
            }
            else if (err) {
                res.send({
                    error: err
                })
            }
            else {
                res.send({
                    results: [],
                    message: "No items"
                })
            }
        });
    },
    deleteCartItem: function (req, res) {
        const id = parseInt(req.params.id);
        const params = {
            user_id: id,
            active: true
        }
        console.log(params)
        Cart.find(params).exec(async function (err, cartItem) {
            console.log(cartItem)
            if (cartItem.length > 0) {
                const updatedCartItem = await Cart.updateOne(params).set({ active: false });
                if (updatedCartItem) {
                    res.send({
                        message: "successfully updated the item in cart",
                    })
                }
                else if (err) {
                    res.send({
                        error: err
                    })
                }
            }
            else {
                res.send({
                    message: 'No Items in cart to update.'
                })
            }
        });
    }
};

