/**
 * UsersController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
    addUser: function (req, res) {
        var params = {
            name: req.body.name,
            userName: req.body.userName,
            emailAddress: req.body.emailAddress,
            accountType: req.body.accountType,

        };
        Users.findOne(params).exec(async function (err, user) {
            if (user) {
                res.send({ message: "User Already Exists", user: user });
            }
            else {
                params['password'] = req.body.password;
                const newUser = await Users.create(params).fetch();
                if (newUser) {
                    let userObject = newUser;
                    delete userObject.password;
                    delete userObject.emailAddress;
                    res.send({
                        message: "Successfully created new user",
                        user: userObject
                    });
                }
                else {
                    res.send({ error: "Something went wrong while creating user" })
                }

            }
        });
    },
    getUserByUserName: async function (req, res) {
        const params = {
            userName: req.body.userName,
        };
        Users.findOne(params).decrypt().exec(await function (err, user) {
            if (user) {
                let userObj = user;
                if (user.password == req.body.password) {
                    delete userObj.password;
                    delete userObj.createdAt;
                    delete userObj.updatedAt;
                    res.send({
                        user: userObj,
                    });
                }
                else {
                    res.send({
                        error: "Invalid Password."
                    });
                }

            }
            else {
                res.send({
                    message: "User not found",
                    user: []
                });
            }
        });
    },
    getUserByUserID: async function (req, res) {
        let user_id = parseInt(req.params.id);
        Users.findOne({ id: user_id }).decrypt().exec(await function (err, user) {
            if (user) {
                let userObj = user;
                delete userObj.password;
                res.send({
                    user: userObj,
                });

            }
            else {
                res.send({
                    message: "User not found",
                    user: []
                });
            }
        });
    }
};

