/**
 * AddressController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
    addAddress: function (req, res) {
        const params = {
            addressType: req.body.addressType,
            user_id: req.body.user_id,
        }
        const addressParams = {
            addressLine1: req.body.addressLine1,
            addressLine2: req.body.addressLine2,
            street: req.body.street,
            locality: req.body.locality,
            city: req.body.city,
            state: req.body.state,
            country: req.body.country,
            postalCode: req.body.postalCode,
        }
        Address.findOne(params).exec(async function (err, address) {
            if (address) {
                const updatedAddress = await Address.updateOne({ id: address.id }).set(addressParams);
                if (updatedAddress) {
                    res.send({
                        message: "Successfully updated your address"
                    })
                }
            }
            else if (err) {
                res.send(
                    { error: err }
                )
            }
            else {
                addressParams.user_id = req.body.user_id;
                addressParams.addressType = req.body.addressType;
                const newAddress = await Address.create(addressParams).fetch();
                if (newAddress) {
                    let data = newAddress;
                    delete newAddress.created_at;
                    delete newAddress.updated_at;
                    res.send({
                        message: 'Sucessfully added address to your profile.',
                        results: data
                    })
                }
                else {
                    res.send({
                        error: 'Something went wrong while inserting new address in table',
                    })
                }
            }
        });
    },
    getAddress: function (req, res) {
        const params = {
            user_id: req.params.id
        };

        Address.find(params).exec(function (err, address) {
            if (address.length > 0) {
                res.send({
                    results: address
                })
            }
            else if (err) {
                res.send({
                    error: err
                })
            }
            else {
                res.send({
                    message: "No address corresponding to this Id is beign recorded."
                })
            }
        });
    },
    updatedAddress: async function (req, res) {
        if (!req.body.id) {
            res.send({
                error: "ID missing in request body."
            });
        }
        const addressParams = {
            id: req.body.id,
        }
        const updateParams = req.body;
        delete updateParams.id;
        const updatedAddress = await Address.updateOne(addressParams).set(updateParams);
        if (updatedAddress) {
            res.send({
                result: updatedAddress
            });
        }
        else {
            res.send({
                message: "No updates to bo performed."
            })
        }
    }
};

