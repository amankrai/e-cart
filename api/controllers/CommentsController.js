/**
 * CommentsController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
    getComments: async function (req, res) {
        let productId = parseInt(req.params.id);
        const param = {
            product_id: productId,
        };
        const commentData = await Comments.find(param).populate('user_id');
        if (commentData.length > 0) {
            res.send({
                results: commentData
            });
        }
        else {
            res.send({
                message: 'No record to display'
            })
        }
    },
    addComment: async function (req, res) {
        let params = {
            product_id: req.body.product_id,
            user_id: req.body.user_id
        }
        Comments.findOne(params).exec(async function (err, comment) {
            if (comment) {
                res.send({ message: "You have already submitted your Review.Thanks" });
                console.log('here', comment)
            }
            else {
                params['data'] = req.body.data;
                params['rating'] = req.body.rating;
                const newComment = await Comments.create(params).fetch();
                if (newComment) {
                    res.send({
                        message: "Thank you for giving your review.",
                        comment: newComment
                    });
                }
                else {
                    res.send({ error: "Something went wrong while creating new comment in db" })
                }

            }
        });
    }

};

